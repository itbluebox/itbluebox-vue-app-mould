import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import router from './router/index'
import  store  from './store/index'
import 'element-plus/theme-chalk/dark/css-vars.css'
import './styles/dark/css-vars.css'
let app = createApp(App)
app.use(router)
app.use(store)
app.mount('#app')
