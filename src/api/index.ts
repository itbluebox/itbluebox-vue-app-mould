import request from '../utils/request'

export const getHomepage = ( ) => {
    return request({
        method: "GET",
        url: "/homepage",
    });
};
export const  getSitePVNum = () =>{
    return request({
        method: "GET",
        url: "/screenApi/siteAnalysis/getSitePVNum",
    });
};
export const  getAllManuscriptCount= () => {
    return request({
        method: "GET",
        url: "/screenApi/siteAnalysis/getAllManuscriptCount",
    });
};
export const   getAllNewManuscript= () => {
    return request({
        method: "GET",
        url: "/screenApi/siteAnalysis/getAllNewManuscript",
    });
};
export const  getHotManuscript= () =>{
    return request({
        method: "GET",
        url: "/screenApi/siteAnalysis/getHotManuscript",
    });
};
export const  getDepartManuscriptNum= () => {
    return request({
        method: "GET",
        url: "/screenApi/siteAnalysis/getDepartManuscriptNum",
    });
};
export const  getDepartManuscriptNumToday= () => {
    return request({
        method: "GET",
        url: "/screenApi/siteAnalysis/getDepartManuscriptNumToday",
    });
};
export const  getReceiptDepartResourceNum= () =>{
    return request({
        method: "GET",
        url: "/screenApi/siteAnalysis/getReceiptDepartResourceNum",
    });
};
export const   getResourceDataNum = () => {
    return request({
        method: "GET",
        url: "/screenApi/siteAnalysis/getResourceDataNum",
    });
};
export const   getAllPV= () =>  {
    return request({
        method: "GET",
        url: "/screenApi/siteAnalysis/getAllPV",
    });
};
export const   getHotSearchWordCloud = () => {
    return request({
        method: "GET",
        url: "/screenApi/siteAnalysis/getHotSearchWordCloud",
    });
};
export const   getScreenList = () =>  {
    return request({
        method: "GET",
        url: "/screenApi/siteAnalysis/getScreenList",
    });
};
export const   getResourceData = () =>  {
    return request({
        method: "GET",
        url: "/screenApi/siteAnalysis/getResourceData",
    });
};

//http://10.12.71.19/screenApi/siteAnalysis/getNewManuscript?siteid=32
export const   getNewManuscript = (params:any) => {
    return request({
        method: "GET",
        url: "/screenApi/siteAnalysis/getNewManuscript",
        params,
    });
};

//http://10.12.71.19/screenApi/siteAnalysis/getManuscriptSituation?siteId=32
export const   getManuscriptSituation  = (params:any) => {
    return request({
        method: "GET",
        url: "/screenApi/siteAnalysis/getManuscriptSituation",
        params,
    });
};
//http://10.12.71.19/screenApi/siteAnalysis/getOneWeekManuscript?siteid=32
export const   getOneWeekManuscript  = (params:any) => {
    return request({
        method: "GET",
        url: "/screenApi/siteAnalysis/getOneWeekManuscript",
        params,
    });
};
export const  getDistribution  = (params:any) =>{
    return request({
        method: "GET",
        url: "/screenApi/siteAnalysis/getDistribution",
        params,
    });
};
export const   getSiteScore = (params:any) => {
    return request({
        method: "GET",
        url: "/screenApi/siteAnalysis/getSiteScore",
        params,
    });
};
export const   getWarningList  = (params:any) =>{
    return request({
        method: "GET",
        url: "/screenApi/siteAnalysis/getWarningList",
        params,
    });
};
export const   getSiteVisit = (params:any) => {
    return request({
        method: "GET",
        url: "/screenApi/siteAnalysis/getSiteVisit",
        params,
    });
};

export const   visitCount  = (params:any) =>{
    return request({
        method: "GET",
        url: "/screenApi/siteAnalysis/visitCount",
        params,
    });
};
export const   getVisitSource = (params:any) => {
    return request({
        method: "GET",
        url: "/screenApi/siteAnalysis/getVisitSource",
        params,
    });
};

