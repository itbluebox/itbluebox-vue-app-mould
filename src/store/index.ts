import { createStore } from 'vuex'
// 创建一个新的 store 实例

interface AppState {
    count?: number;
    title?: string;
}

const store = createStore({
    state: (): AppState => ({
        count: 0,
        title: "itbluebox蓝盒子",
    }),
    mutations: {
        increment (state,payload) {
             state.count+=payload
        },
        inTitle (state,name) {
            state.title =name
        }
    },
    getters:{
        totalPrice(state){
            return  state.count
        },
        getTitle(state){
            return  state.title;
        }
    },
    actions:{
        asyncAdd(store,payload){
            setTimeout(()=>{
                store.commit('increment',10)
            },1000)
        },
        setTitle(store,name){
            store.commit('inTitle',name)
        }
    }
})

export default store;

