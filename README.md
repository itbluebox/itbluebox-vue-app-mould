# Vue 3 + Vite

<div align="center">
    <h1>itbluebox-vue-app</h1>
    <p>vite + vue3 + ts 开箱即用现代开发模板 🐳 </p>
</div>

<br />
<br />

## 特点 🐳

1. Vite 的
2. Vue3 的
3. 文件路由
4. 布局系统
6. Api 自动引入
7. 组件自动引入
8. 图标自动引入
9. VueUse 支持
10. TypeScript 的
11. Windi CSS 的
12. 暗黑模式支持
13. axios 请求支持
14. Vuex 状态管理
15. npm 包管理器
25. tsx 支持
27. 环境变量配置支持
28. 统一的代码规范与风格支持
29. 生产环境自动移除开发日志

## `node` 版本推荐 🐎

因为该模板完全面向现代，所以推荐大家使用 `node` 当前的长期维护版本 `v16`， 大于 `v16.13.1` 即可。

<br />
<br />
<br />

## 使用 🐂

> 该模板不限定大家使用某一特定的包管理器，npm，yarn 和 pnpm 都行。同时注意 npm 的版本应该尽量的新。


1. 安装依赖

```shell
npm install
```
2. 开发
```shell
npm run dev
```
3. 预览

```shell
npm run dev

```

4. 打包

```shell
npm run build
```


